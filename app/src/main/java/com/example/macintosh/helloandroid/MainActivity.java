package com.example.macintosh.helloandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    //instance to save the state of the image clicked or not
    //it starts off as false
    Boolean udacitypressed = false;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.text);

    }


    /**
     * showDetails method gets triggered upon clicking the
     * Udacity image. Displays/Hides text based on the state
     * of the image click: if pressed then it hides, vice versa
     * */
    public void showDetails(View view){
        if(udacitypressed){
            textView.setText("");
            udacitypressed = false;
        }
        else{

            //toString method gets called to display the text
            textView.setText(toString());
            udacitypressed = true;
        }
    }

    /**
     * This method gets called from the showDetails() method
     * it simply returns or outputs udacity details
     * */
    @Override
    public String toString() {
        String details = "Udacity\n" +
                "\n" +
                "2465 Latham St\n" +
                "\n" +
                "Mountain View, CA 94043\n" +
                "\n" +
                "650-555-5555";

        return details;
    }
}
